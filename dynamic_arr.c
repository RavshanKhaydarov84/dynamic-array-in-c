#include <stdio.h>
#include <string.h>

int main()
{
    int* arr;
    int i=0,j=0, temp=0;
    char s1[21]={0x00,};

lbl_input:
    printf("\nInput count of array elements:\n");
    // Inputting size of dynamic array
    gets(&s1);
    int n = atoi(s1);
    if(n < 1){
        printf("\nNumber should be natural number! Please try again!");
        goto lbl_input;
    }
    int k=0,l=0;
    // Inputting an array
    for (i=0; i<n; i++) {
        printf("\narray[%d]=", i);
        gets(&s1);
        l = atoi(s1);
        if(k == 0){
            arr = (int*)malloc(sizeof(int));
        }else{
            arr = (int*)realloc(arr, sizeof(int)*(k+1));
        }
        *(arr+k)=l;
        k++;
    }
    // Printing inputted array in order to imagine
    printf("\nArray you've inputted is:\n");
    for(i=0; i<k; i++){
        printf("arr[%d]=[%d]\n", i, *(arr+i));
    }
    // Sorting an array
    for(i=0; i<k-1; i++){
    	for(j=i+1; j<k; j++ ){
    		if(*(arr+i) > *(arr+j)){
    			temp = *(arr+i);
    			*(arr+i) = *(arr+j);
    			*(arr+j) = temp;
    		}
    	}
    }
    //Printing a sorted array
    printf("\nAnd sorted array is:\n");
    for(i=0; i<k; i++){
        printf("arr[%d]=[%d]\n", i, *(arr+i));
    }
    free(arr);
    gets((char*)&s1);//Will wait for pressing any key by user
    return 0;
}
